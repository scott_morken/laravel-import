<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 3/27/14
 * Time: 12:47 PM
 */

class HandlerTest extends TestCase {

    /**
     * @var \Smorken\Import\Handler
     */
    protected $sut;

    public function setUp()
    {
        parent::setUp();
        $this->sut = $this->app['smorken.import'];
    }

    public function testOutputResults()
    {
        $this->sut->setActions(array(
            'foo' => array(
                'to' => array(
                    'cls' => 'bar',
                ),
                'from' => array(
                    'cls' => 'baz',
                )
            )
        ));
        $expected = "** 2";
        $r = $this->sut->outputResults();
        $this->assertStringStartsWith($expected, $r);
    }
} 