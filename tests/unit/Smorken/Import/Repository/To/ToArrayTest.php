<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 3/27/14
 * Time: 11:07 AM
 */

use Smorken\Import\Repository\To\Arr\ToArray;

class ToArrayTest extends TestCase {
    /**
     * @var \Smorken\Import\Repository\To\Arr\ToArray
     */
    protected $sut;

    public function setUp()
    {
        parent::setUp();
        $this->sut = new ToArray();
    }

    public function testGetKeys()
    {
        ToArray::$keys = array('id1');
        $keys = $this->sut->getKeys();
        $this->assertEquals(array('id1'), $keys);
    }

    public function testCreateUpdateOne()
    {
        ToArray::$keys = array('id1');
        $data = array('test1' => 1, 'test2' => 2, 'id1' => '_1');
        $this->sut->createOrUpdate($this->sut->getKeys(), $data);
        $this->assertEquals($data, $this->sut->attributes['_1']);
    }

    public function testCreateUpdateOneCompoundKey()
    {
        ToArray::$keys = array('id1', 'id2');
        $data = array('test1' => 1, 'test2' => 2, 'id1' => '_1', 'id2' => '_2');
        $this->sut->createOrUpdate($this->sut->getKeys(), $data);
        $this->assertEquals($data, $this->sut->attributes['_1+_2']);
    }

    public function testCreateUpdateMultiple()
    {
        ToArray::$keys = array('id1', 'id2');
        $data = array(
            array('test1' => 1, 'test2' => 2, 'id1' => '_1', 'id2' => '_2'),
            array('test1' => 44, 'test2' => 44, 'id1' => 'foo', 'id2' => 'bar'),
        );
        foreach($data as $d) {
            $this->sut->createOrUpdate($this->sut->getKeys(), $d);
        }
        $this->assertEquals($data[1], $this->sut->attributes['foo+bar']);
    }

    public function testCreateUpdateMultipleWithOverwrite()
    {
        ToArray::$keys = array('id1', 'id2');
        $data = array(
            array('test1' => 1, 'test2' => 2, 'id1' => '_1', 'id2' => '_2'),
            array('test1' => 44, 'test2' => 44, 'id1' => 'foo', 'id2' => 'bar'),
            array('test1' => 'foo', 'test2' => 'bar', 'id1' => '_1', 'id2' => '_2'),
        );
        foreach($data as $d) {
            $this->sut->createOrUpdate($this->sut->getKeys(), $d);
        }
        $this->assertEquals($data[2], $this->sut->attributes['_1+_2']);
    }
}
 