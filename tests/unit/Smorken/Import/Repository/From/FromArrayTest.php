<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 3/27/14
 * Time: 11:18 AM
 */

use Mockery as m;
use Smorken\Import\Repository\From\Arr\FromArray;

class FromArrayTest extends TestCase {
    /**
     * @var Smorken\Import\Repository\From\Arr\FromArray
     */
    protected $sut;
    /**
     * @var Mockery/Mock
     */
    protected $to;

    public function setUp()
    {
        parent::setUp();
        $this->to = m::mock('Smorken\Import\Repository\To\ToRepositoryInterface');
        $this->sut = new FromArray();
    }

    public function tearDown()
    {
        m::close();
    }

    /**
     * @expectedException \ErrorException
     */
    public function testSetStorageFails()
    {
        $storage = m::mock('stdClass');
        $this->sut->setStorage($storage);
    }

    public function testProcessRow()
    {
        $this->sut->setConversion($this->getConversion());
        $row = $this->sut->processRow($this->getData()[0]);
        $expected = array('test_to' => 1, 'to' => 'foo');
        $this->assertEquals($expected, $row);
    }

    public function testProcessRowNoMethodExists()
    {
        $this->sut->setConversion($this->getConversion());
        $row = $this->sut->processRow($this->getData()[1]);
        $expected = array('test_to' => 2, 'to' => 'bar', '__func:method' => 'baz');
        $this->assertEquals($expected, $row);
    }

    public function testProcessResults()
    {
        $this->sut->setConversion($this->getConversion());
        $this->to->shouldReceive('createOrUpdate')
            ->times(2)
            ->andReturn(true);
        $this->to->shouldReceive('getKeys')
            ->times(2)
            ->andReturn('from');
        $this->sut->setStorage($this->to);
        $this->sut->processResults($this->getData());
        $this->assertEquals(0, $this->sut->hasErrors());
    }

    public function testProcessResultsWithErrors()
    {
        $this->sut->setConversion($this->getConversion());
        $this->to->shouldReceive('createOrUpdate')
            ->times(2)
            ->andReturn(false);
        $this->to->shouldReceive('getKeys')
            ->times(2)
            ->andReturn('from');
        $this->sut->setStorage($this->to);
        $this->sut->processResults($this->getData());
        $this->assertEquals(2, $this->sut->hasErrors());
    }

    protected function getConversion()
    {
        return array(
            'test_from' => 'test_to',
            'from' => 'to',
            'another' => '__func:method',
        );
    }

    protected function getData()
    {
        return array(
            array(
            'test_from' => 1,
            'from' => 'foo',
            ),
            array(
                'test_from' => 2,
                'from' => 'bar',
                'another' => 'baz',
            ),
        );
    }
}
