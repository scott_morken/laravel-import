<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 3/27/14
 * Time: 1:11 PM
 */
use Smorken\Import\Repository\From\AbstractEloquentFrom;
use Mockery as m;

class EloquentFrom extends AbstractEloquentFrom {

}

class EloquentFromTest extends TestCase {

    /**
     * @var Mockery/Mock
     */
    protected $to;

    /**
     * @var EloquentFrom
     */
    protected $sut;

    public function setUp()
    {
        parent::setUp();
        $this->to = m::mock('Smorken\Import\Repository\To\ToRepositoryInterface');
        $this->sut = new EloquentFrom();
    }


}
 