<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 3/27/14
 * Time: 11:51 AM
 */

use Smorken\Import\Repository\From\Arr\FromArray;
use Smorken\Import\Repository\To\Arr\ToArray;

class ArrayImportTest extends TestCase {
    /**
     * @var Smorken\Import\Repository\From\Arr\FromArray
     */
    protected $from;
    /**
     * @var Smorken\Import\Repository\To\Arr\ToArray
     */
    protected $to;

    public function setUp()
    {
        parent::setUp();
        $this->from = new FromArray();
        $this->from->setConversion($this->getConversion());
        $this->to = new ToArray();
        ToArray::$keys = array('to_id');
    }

    public function testLoadValid()
    {
        $this->from->setData($this->getData());
        $this->from->run($this->to);
        $this->assertEquals(2, count($this->to->attributes));
        $this->assertArrayHasKey('myid1', $this->to->attributes);
    }

    protected function getConversion()
    {
        return array(
            'id' => 'to_id',
            'test_from' => 'test_to',
            'from' => 'to',
            'another' => '__func:method',
        );
    }

    protected function getData()
    {
        return array(
            array(
                'id' => 'myid1',
                'test_from' => 1,
                'from' => 'foo',
            ),
            array(
                'id' => 'myid2',
                'test_from' => 2,
                'from' => 'bar',
                'another' => 'baz',
            ),
        );
    }
} 