<?php namespace Smorken\Import;

use Illuminate\Support\ServiceProvider;

use Smorken\Import\Result\Handler\File;

class ImportServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('smorken/import');
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app['smorken.import'] = $this->app->share(function($app) {
            return new Handler($app['config']->get('import::actions'), $app['smorken.import.handler']);
        });
        $this->app['smorken.import.handler'] = $this->app->share(function($app) {
            $f = new File();
            $f->init($app['config']->get('import::time_file'), $app['config']->get('import::use_time'));
            return $f;
        });
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array('smorken.import');
	}

}
