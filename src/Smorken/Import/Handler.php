<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 3/27/14
 * Time: 12:42 PM
 */

namespace Smorken\Import;

use Smorken\Import\Result\Handler\ResultHandlerInterface;

class Handler
{
    protected $output = false;

    protected $actions = array();

    protected $results = array();

    protected $start;
    protected $end;

    /**
     * @var Result\Handler\ResultHandlerInterface
     */
    protected $fileHandler;

    protected $previousResults = array();

    protected $return = 0;

    protected $addData = array();

    public function __construct($actions = array(), ResultHandlerInterface $fileHandler = null)
    {
        $this->setActions($actions);
        if (\Config::get('import::use_time') && $fileHandler) {
            $this->fileHandler = $fileHandler;
            $this->previousResults = $this->fileHandler->retrieve();
        }
    }

    /**
     * @param bool $full
     * @param array|string|null $runonly
     */
    public function run($full = false, $runonly = null, $output = false)
    {
        $this->output = $output;
        $this->init();
        if ($runonly && !is_array($runonly)) {
            $runonly = array($runonly);
        } else if (!$runonly) {
            $runonly = array_keys($this->actions);
        }
        $this->setResults($runonly);
        $this->outputHeader();
        foreach ($runonly as $action) {
            $this->outputActionHeader($action);
            $this->handleAction($action, $full);
            $this->outputRow($action, 'time');
        }
        $this->close();
        $this->outputFooter();
        return $this->return;
    }

    public function addData($data)
    {
        $this->addData = $data;
    }

    protected function init()
    {
        $this->start = microtime(true);
    }

    protected function close()
    {
        $this->end = microtime(true);
        if ($this->fileHandler) {
            $this->fileHandler->write($this->results);
        }
    }

    protected function handleAction($action, $full = false)
    {
        if (isset($this->actions[$action])) {
            $onlyFull = isset($this->actions[$action]['only_full']) && $this->actions[$action]['only_full'] ? true : false;
            if (!$full && $onlyFull) {
                return;
            }
            $to_config = $this->actions[$action]['to'];
            $from_config = $this->actions[$action]['from'];

            $to = $this->getTo($to_config);
            $from = $this->getFrom($from_config);
            $this->setupFrom($action, $from, $from_config, $full);
            $this->_run($action, $to, $to_config, $from, $from_config);
        } else {
            $this->setErrors($action, -1);
        }
    }

    protected function setupFrom($action, $from, &$from_config, $full)
    {
        if (isset($from_config['conversion'])) {
            $from->setConversion($from_config['conversion']);
        }
        if (isset($from_config['additional_processing'])) {
            $from->setAdditionalProcessing($from_config['additional_processing']);
        }
        if ($this->addData) {
            $from->addData($this->addData);
        }
        $from_config = $this->handleLoading($action, $from, $from_config, $full);
    }

    protected function handleLoading($action, $from, $from_config, $full = false)
    {
        if (isset($from_config['time_comp_field'])) {
            $from->setTimeCompField($from_config['time_comp_field']);
        }
        if (!$full && $field = $from->getTimeCompField()) {
            if (isset($this->previousResults[$action])) {
                try {
                    $vo = $this->previousResults[$action];
                    $last_start = $vo->start;
                    $updated = false;
                    if (isset($from_config['criteria']['where'])) {
                        foreach($from_config['criteria']['where'] as $i => $k) {
                            if ($k[0] == $field) {
                                $from_config['criteria']['where'][$i] = array($field, '>=', date('Y-m-d H:i:s', $last_start));
                                $updated = true;
                            }
                        }
                    }
                    if (!$updated) {
                        $from_config['criteria']['where'][] = array($field, '>=', date('Y-m-d H:i:s', $last_start));
                    }
                } catch (\Exception $e) {
                    $from_config['criteria']['fullLoad'] = array();
                }
            }
        }
        return $from_config;
    }

    /**
     * @param string $action
     * @param \Smorken\Import\Repository\To\ToRepositoryInterface $to
     * @param array $to_config
     * @param \Smorken\Import\Repository\From\FromRepositoryInterface $from
     * @param array $from_config
     */
    protected function _run($action, $to, $to_config, $from, $from_config)
    {
        $this->setStart($action);
        $count = $from->run($to, isset($from_config['criteria']) ? $from_config['criteria'] : array());
        $this->setErrors($action, $from->hasErrors());
        $this->setCount($action, $count);
        $this->setEnd($action);
        if ($from->hasErrors()) {
            $this->return = -1;
        }
    }

    public function outputResults()
    {
        $this->output = true;
        $this->outputHeader();
        foreach ($this->results as $action => $vo) {
            $this->outputActionHeader($action);
            foreach ($vo->attributes as $k) {
                $this->outputRow($action, $k);
            }
        }
        $this->outputFooter();
    }

    public function outputHeader()
    {
        if ($this->output) {
            $r = str_pad(' ' . date('Y-m-d H:i:s', $this->start) . ' ', 40, '*', STR_PAD_BOTH) . PHP_EOL;
            echo $r;
        }
        return false;
    }

    public function outputActionHeader($action)
    {
        if ($this->output) {
            echo "$action" . PHP_EOL;
        }
        return false;
    }

    public function outputFooter()
    {
        if ($this->output) {
            $r = str_pad(' MEM USED: ' . memory_get_peak_usage(true) / (1024 * 1000) . "MB ", 40, '*', STR_PAD_BOTH) . PHP_EOL;
            $r .= str_pad(' TIME: ' . number_format(($this->end - $this->start), 4) . ' s ', 40, '*', STR_PAD_BOTH) . PHP_EOL;
            echo $r;
        }
        return false;
    }

    public function outputRow($action, $attribute, $spacer = ' ')
    {
        if ($this->output) {
            $r = '';
            if (isset($this->results[$action]) && property_exists($this->results[$action], $attribute)) {
                $vo = $this->results[$action];

                $methodname = 'get' . ucfirst($attribute);
                if (method_exists($vo, $methodname)) {
                    $v = $vo->$methodname();
                } else {
                    $v = $vo->$attribute;
                }
                $r = $spacer . str_pad($attribute, 10, '-', STR_PAD_LEFT) . ': ';
                if (is_array($v)) {
                    $r .= PHP_EOL;
                    foreach($v as $k => $nv) {
                        $r .= str_pad($k, 15, ' ', STR_PAD_LEFT) . ': ';
                        if (is_array($nv)) {
                            $r .= implode(', ', array_map(function($key) use ($nv) { return "$key: ${nv[$key]}"; }, array_keys($nv)));
                        }
                        else {
                            $r .= $nv;
                        }
                        $r .= PHP_EOL;
                    }
                }
                else {
                    $r .= $v . PHP_EOL;
                }
            }
            echo $r;
        }
        return false;
    }

    protected function getTo($config)
    {
        return $this->getBackendHandler('to', $config);
    }

    protected function getFrom($config)
    {
        return $this->getBackendHandler('from', $config);
    }

    protected function getBackendHandler($type, $config)
    {
        if (!isset($config['cls'])) {
            throw new HandlerException("cls key not found in config for $type object");
        }
        return new $config['cls']();
    }

    public function actions()
    {
        return $this->actions;
    }

    public function setActions($actions = array())
    {
        $this->actions = $actions;
    }

    public function results()
    {
        return $this->results;
    }

    public function setResults($actions)
    {
        $this->results = array();
        foreach ($actions as $name) {
            $this->results[$name] = new Result\VO();
        }
    }

    public function setErrors($action, $value)
    {
        $this->setResultValue($action, 'errors', $value);
        $this->outputRow($action, 'errors');
    }

    public function setCount($action, $value)
    {
        $this->setResultValue($action, 'count', $value);
        $this->outputRow($action, 'count');
    }

    public function setStart($action)
    {
        $this->setResultValue($action, 'start', microtime(true));
        $this->outputRow($action, 'start');
    }

    public function setEnd($action)
    {
        $this->setResultValue($action, 'end', microtime(true));
        $this->outputRow($action, 'end');
    }

    public function setResultValue($action, $type, $value)
    {
        $this->results[$action]->$type = $value;
    }
} 