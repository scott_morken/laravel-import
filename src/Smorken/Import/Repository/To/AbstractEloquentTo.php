<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 3/27/14
 * Time: 10:12 AM
 */

namespace Smorken\Import\Repository\To;

use Smorken\Ext\Database\Eloquent\Model;

abstract class AbstractEloquentTo extends Model {

    use ToTraits;

    protected function _createOrUpdate($keys, $data)
    {
        try {
            $r = $this->createOrUpdate($keys, $data);
            if (!$r) {
                $errs = \DB::connection($this->getConnectionName())->getPdo()->errorInfo();
                if ($errs[0] !== 0) {
                    $this->errors ++;
                    $this->last_error = $errs[2];
                }
            }
            return $r;
        }
        catch (\Exception $e) {
            $this->errors ++;
            $this->last_error = $e->getMessage();
            return false;
        }
    }
}