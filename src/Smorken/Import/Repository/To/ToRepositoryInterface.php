<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 3/27/14
 * Time: 9:39 AM
 */

namespace Smorken\Import\Repository\To;


interface ToRepositoryInterface {

    /**
     * @param string|array $keys
     * @param array $data
     * @return bool
     */
    public function doCreateOrUpdate($keys, array $data);

    /**
     * @return array
     */
    public function getKeys();

    public function hasErrors();

    public function getLastError();
} 