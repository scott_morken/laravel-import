<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 3/27/14
 * Time: 11:57 AM
 */

namespace Smorken\Import\Repository\To;


trait ToTraits {

    protected $errors = 0;

    protected $last_error = null;

    public static $keys = array();

    /**
     * @return array
     */
    public function getKeys()
    {
        return static::$keys;
    }

    public function hasErrors()
    {
        return $this->errors;
    }

    public function getLastError()
    {
        return $this->last_error;
    }

    public function doCreateOrUpdate($keys, array $data)
    {
        $this->last_error = null;
        return $this->_createOrUpdate($keys, $data);
    }

    abstract protected function _createOrUpdate($keys, $data);
} 