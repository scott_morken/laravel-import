<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 3/27/14
 * Time: 10:43 AM
 */

namespace Smorken\Import\Repository\To\Arr;

use Smorken\Import\Repository\To\ToRepositoryInterface;
use Smorken\Import\Repository\To\ToTraits;

class ToArray implements ToRepositoryInterface {

    use ToTraits;

    public $attributes = array();

    protected function _createOrUpdate($keys, $data)
    {
        if (!is_array($keys)) {
            $keys = array($keys);
        }
        $k = array();
        foreach($keys as $key) {
            $k[] = $data[$key];
        }
        $k = implode('+', $k);
        $this->attributes[$k] = $data;
        return true;
    }
}