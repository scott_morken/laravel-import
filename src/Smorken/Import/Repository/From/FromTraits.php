<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 3/27/14
 * Time: 11:35 AM
 */

namespace Smorken\Import\Repository\From;

use Smorken\Import\Repository\To\ToRepositoryInterface;
use Illuminate\Support\MessageBag;

trait FromTraits
{
    /**
     * @var \Smorken\Import\Repository\To\ToRepositoryInterface $storage
     */
    protected $storage;

    protected $conversion = array();

    /**
     * @var array of function names to receive ::func(&$new_row, $row)
     */
    protected $additionalProcessing = array();

    public $errors = 0;

    protected $errMessages;

    protected $addData;

    protected $timeCompField;

    public function setTimeCompField($timeCompField)
    {
        $this->timeCompField = $timeCompField;
    }

    public function getTimeCompField()
    {
        return $this->timeCompField;
    }

    public function setConversion($conversion)
    {
        $this->conversion = $conversion;
    }

    public function setAdditionalProcessing($processing)
    {
        $this->additionalProcessing = $processing;
    }

    public function setStorage(ToRepositoryInterface $storage)
    {
        $this->storage = $storage;
    }

    /**
     * Iterate over the iterator from load
     * @param $results
     * @return integer
     */
    public function processResults($results)
    {
        $count = 0;
        foreach ($results as $row) {
            $count++;
            $row = $this->preProcessRow($row);
            $row = $this->processRow($row);
            $row = $this->postProcessRow($row);
            if ($row) {
                if (!$this->storeRow($row)) {
                    $last = $this->storage->getLastError();
                    if ($last) {
                        \Log::error($last);
                        $this->addErrorMessage('store', $last);
                    }
                    $this->errors++;
                }
            }
        }
        return $count;
    }

    /**
     * Change the row into a compatible format for storage
     * if $row is an object, it needs to have a ::toArray method
     * @param mixed $row
     * @return mixed
     */
    public function processRow($row)
    {
        if (!is_array($row)) {
            $row = $row->toArray();
        }
        $nrow = array();
        if (!$this->conversion) {
            $this->createConversionFromRowKeys(array_keys($row));
        }
        foreach ($this->conversion as $from => $to) {
            $action = $this->conversion[$from];
            $value = null;
            if (substr($action, 0, 7) == '__func:') {
                try {
                    list($action, $value) = call_user_func(array($this, substr($action, 7)), $from, $row);
                } catch (\Exception $e) {
                    \Log::error($e);
                }
            } else {
                $value = isset($row[$from]) ? $row[$from] : null;
            }
            $nrow[$action] = $value;
        }
        if ($this->additionalProcessing && !empty($this->additionalProcessing)) {
            foreach ($this->additionalProcessing as $func) {
                call_user_func(array($this, $func), $nrow, $row);
            }
        }
        return $nrow;
    }

    public function preProcessRow($row)
    {
        return $row;
    }

    public function postProcessRow($row)
    {
        return $row;
    }

    /**
     * Stores the row to the storage backend
     * @param $row
     * @return mixed
     */
    public function storeRow($row)
    {
        return $this->storage->createOrUpdate($this->storage->getKeys(), $row);
    }

    public function hasErrors()
    {
        return $this->errors;
    }

    public function getErrorMessages()
    {
        return $this->errMessages;
    }

    public function addErrorMessage($key, $message)
    {
        if (!$this->errMessages) {
            $this->errMessages = new MessageBag();
        }
        $key = get_class($this) . '::' . $key;
        $this->errMessages->add($key, $message);
    }

    public function addData($data)
    {
        $this->addData = $data;
    }

    protected function createConversionFromRowKeys($keys)
    {
        foreach($keys as $key) {
            $this->conversion[$key] = $key;
        }
    }
} 