<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 6/18/14
 * Time: 2:13 PM
 */

namespace Smorken\Import\Repository\From;


use Smorken\Import\Repository\To\ToRepositoryInterface;

abstract class AbstractDbFrom {

    use FromTraits;

    protected $connection = 'db';
    protected $table = null;

    /**
     * @param \Smorken\Import\Repository\To\ToRepositoryInterface $storage
     * @param array $criteria
     * @return integer
     */
    public function run(ToRepositoryInterface $storage, array $criteria = array())
    {
        \DB::disableQueryLog();
        $this->setStorage($storage);
        $me = $this;
        $count = 0;
        $this->getQuery($criteria)->chunk(1000, function($results) use (&$me, &$count) {
            $ncount = $me->processResults($results);
            if (is_integer($ncount)) {
                $count += $ncount;
            }
            else {
                $count = $ncount;
            }
        });
        \DB::enableQueryLog();
        return $count;
    }

    public function getQuery($criteria)
    {
        $query = $this->getBaseQuery($criteria);
        return $this->_getQuery($query);
    }

    abstract protected function _getQuery($query);

    protected function getBaseQuery($criteria)
    {
        $query = \DB::connection($this->connection)->table($this->table);
        foreach($criteria as $what => $items) {
            $this->addCriteria($what, $items, $query);
        }
        return $query;
    }

    protected function addCriteria($what, $items, $model)
    {
        if (!$items) {
            return call_user_func(array($model, $what));
        }
        foreach($items as $item) {
            if (is_array($item)) {
                call_user_func_array(array($model, $what), $item);
            }
            else {
                if ($item) {
                    call_user_func(array($model, $what), $item);
                }
                else {
                    call_user_func(array($model, $what));
                }
            }
        }
    }
} 