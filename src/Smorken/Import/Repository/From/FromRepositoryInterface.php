<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 3/27/14
 * Time: 9:41 AM
 */

namespace Smorken\Import\Repository\From;

use Smorken\Import\Repository\To\ToRepositoryInterface;

interface FromRepositoryInterface {

    /**
     * Handles loading the results.
     * @param \Smorken\Import\Repository\To\ToRepositoryInterface $storage
     * @param array $criteria
     * @return \Illuminate\Database\Eloquent\Model|Model
     */
    public function run(ToRepositoryInterface $storage, array $criteria = array());

    /**
     * Iterate over the iterator from load
     * @param $results
     * @return mixed
     */
    public function processResults($results);

    /**
     * Change the row into a compatible format for storage
     * @param $row
     * @return mixed
     */
    public function processRow($row);

    /**
     * @param $row
     * @return mixed
     */
    public function preProcessRow($row);

    /**
     * @param $row
     * @return mixed
     */
    public function postProcessRow($row);

    /**
     * Stores the row to the storage backend
     * @param $row
     * @return mixed
     */
    public function storeRow($row);

    /**
     * Returns the field name of the time comparison field
     * @return string|null
     */
    public function getTimeCompField();

    /**
     * @return integer count of errors
     */
    public function hasErrors();

    /**
     * @return Illuminate\Support\MessageBag
     */
    public function getErrorMessages();

    /**
     * @param string $key
     * @param string $message
     * @return null
     */
    public function addErrorMessage($key, $message);

    public function addData($data);
} 