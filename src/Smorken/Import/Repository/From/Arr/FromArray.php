<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 3/27/14
 * Time: 10:47 AM
 */

namespace Smorken\Import\Repository\From\Arr;

use Smorken\Import\Repository\From\FromRepositoryInterface;
use Smorken\Import\Repository\From\FromTraits;
use Smorken\Import\Repository\To\ToRepositoryInterface;

class FromArray implements FromRepositoryInterface {

    use FromTraits;

    public $data = array();

    public function setData($array)
    {
        $this->data = $array;
    }

    public function data()
    {
        return $this->data;
    }

    /**
     * Handles loading the results.
     * @param \Smorken\Import\Repository\To\ToRepositoryInterface $storage
     * @param array $criteria
     * @return integer
     */
    public function run(ToRepositoryInterface $storage, array $criteria = array())
    {
        \DB::disableQueryLog();
        $this->setStorage($storage);
        $me = $this;
        $count = 0;
        $count += $me->processResults($this->data());
        \Event::fire('import.count', $count);
        \DB::enableQueryLog();
        return $count;
    }
}