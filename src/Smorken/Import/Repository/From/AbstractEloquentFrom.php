<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 3/27/14
 * Time: 9:43 AM
 */

namespace Smorken\Import\Repository\From;

use Smorken\Ext\Database\Eloquent\Model;
use Smorken\Utils\Utils;
use Smorken\Import\Repository\To\ToRepositoryInterface;

abstract class AbstractEloquentFrom extends Model {

    use FromTraits;

    public static function defaultCriteria()
    {
        return array();
    }

    /**
     * @param \Smorken\Import\Repository\To\ToRepositoryInterface $storage
     * @param array $criteria
     * @return integer
     */
    public function run(ToRepositoryInterface $storage, array $criteria = array())
    {
        \DB::disableQueryLog();
        $this->setStorage($storage);
        $me = $this;
        $count = 0;
        $this->make($criteria)->chunk(1000, function($results) use (&$me, &$count) {
            $count += $me->processResults($results);
        });
        \DB::enableQueryLog();
        return $count;
    }

    /**
     * Make a new instance of the entity to query on
     *
     * @param array $criteria
     * @return \Smorken\Ext\Database\Eloquent\Model
     */
    public function make(array $criteria = array())
    {
        if (empty($criteria)) {
            $criteria = static::defaultCriteria();
        }
        $model = $this->newQuery();
        foreach($criteria as $what => $items) {
            $this->addCriteria($what, $items, $model);
        }
        return $model;
    }

    protected function addCriteria($what, $items, $model)
    {
        if (!$items) {
            return call_user_func(array($model, $what));
        }
        foreach($items as $item) {
            if (is_array($item)) {
                call_user_func_array(array($model, $what), $item);
            }
            else {
                if ($item) {
                    call_user_func(array($model, $what), $item);
                }
                else {
                    call_user_func(array($model, $what));
                }
            }
        }
    }

    public function scopeFullLoad($query)
    {
        return $query;
    }
} 