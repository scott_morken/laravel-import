<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 3/28/14
 * Time: 11:28 AM
 */

namespace Smorken\Import\Result;


class VO {
    public $start;
    public $end;
    public $errors = 0;
    public $count = 0;
    public $time = 0;

    public $attributes = array('count', 'errors', 'start', 'end', 'time');

    public function getStart()
    {
        return $this->convertToHumanReadable($this->start);
    }

    public function getEnd()
    {
        return $this->convertToHumanReadable($this->end);
    }

    public function getTime()
    {
        return number_format($this->end - $this->start, 4) . ' s';
    }

    protected function convertToHumanReadable($timestamp)
    {
        if ($timestamp) {
            $dt = new \DateTime();
            $dt->setTimestamp($timestamp);
            return $dt->format('H:i:s.') . last(explode('.', $timestamp));
        }
        return null;
    }
} 