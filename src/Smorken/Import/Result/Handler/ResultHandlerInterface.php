<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 3/28/14
 * Time: 11:40 AM
 */

namespace Smorken\Import\Result\Handler;


interface ResultHandlerInterface {

    public function init($backend, $active = true);

    public function write($data);

    public function retrieve();
} 