<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 3/28/14
 * Time: 11:37 AM
 */

namespace Smorken\Import\Result\Handler;

use Illuminate\Filesystem\Filesystem;

class File extends Filesystem implements ResultHandlerInterface
{

    protected $backend;

    protected $active = true;

    public function init($backend, $active = true)
    {
        $this->backend = $backend;
        $this->active = $active;
    }

    public function write($data)
    {
        if ($this->active) {
            $s = serialize($data);
            $this->put($this->backend, $s);
        }
    }

    public function retrieve()
    {
        if ($this->active && $this->exists($this->backend)) {
            $f = $this->get($this->backend);
            return unserialize($f);
        }
    }
} 