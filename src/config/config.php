<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 3/26/14
 * Time: 2:45 PM
 */
return array(
    'actions' => array(
        'external_person' => array(
            'to' => array(
                'cls' => '',
                'options' => array(),
            ),
            'from' => array(
                'cls' => '',
                'options' => array(),
            )
        )
    ),
    'use_time' => true,
    'time_file' => storage_path() . DIRECTORY_SEPARATOR . 'import.times.obj'
);